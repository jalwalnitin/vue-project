import userManager from "@/utils/userDetails.js";

const deleteComment = async (props, onSuccess, onFailure) => {
  const response = await fetch(
    "https://conduit.productionready.io/api/articles/" +
      props.slugId +
      "/comments/" +
      props.commentId,
    {
      method: "DELETE",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + userManager.user.value.token,
      },
    }
  );

  if (response.ok) {
    if (onSuccess) {
      onSuccess(response);
    }
  } else {
    if (onFailure) {
      onFailure(response);
    } else {
      throw Error(await response.json());
    }
  }
};

const postComment = async (args, onSuccess, onFailure) => {
  const response = await fetch(
    "https://conduit.productionready.io/api/articles/" +
      args.slugId +
      "/comments",
    {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + userManager.user.value.token,
      },
      body: JSON.stringify({
        comment: {
          body: args.body,
        },
      }),
    }
  );

  if (response.ok) {
    if (onSuccess) {
      onSuccess(response);
    }
  } else {
    if (onFailure) {
      onFailure(response);
    } else {
      throw Error(await response.json());
    }
  }
};

export default { deleteComment, postComment };
