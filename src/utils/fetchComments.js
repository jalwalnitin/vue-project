const fetchComments = async (articleId, onSuccess) => {
  let response = await fetch(
    "https://conduit.productionready.io/api/articles/" + articleId + "/comments"
  );

  if (response.ok) {
    response = await response.json();

    onSuccess(response.comments);
  } else {
    throw Error("Could not fetch comments.");
  }
};

export default fetchComments;
