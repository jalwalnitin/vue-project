const articlesRequest = async (args) => {
  const response = await fetch(
    "https://conduit.productionready.io/api/articles" + args
  );

  if (response.ok) {
    return await response.json();
  } else {
    throw Error(response);
  }
};

export default articlesRequest;
