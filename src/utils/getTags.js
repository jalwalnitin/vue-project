const tagsRequest = async () => {
  const response = await fetch("https://conduit.productionready.io/api/tags");

  if (response.ok) {
    return await response.json();
  } else {
    throw Error(response);
  }
};

export default tagsRequest;
