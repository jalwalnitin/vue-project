import userManager from "@/utils/userDetails.js";

const requestArticlePublish = async (content) => {
  const response = await fetch(
    "https://conduit.productionready.io/api/articles",
    {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Token " + userManager.user.value.token,
      },
      body: JSON.stringify(content),
    }
  );

  if (response.ok) {
    return await response.json();
  } else {
    throw Error(response);
  }
};

export default requestArticlePublish;
