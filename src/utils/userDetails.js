import { ref } from "vue";

const userManager = (() => {
  let user = ref("");

  const setUser = (details) => {
    sessionStorage.setItem("userdetails", JSON.stringify(details));
    user.value = details;
  };

  const getUser = () => {
    user.value = JSON.parse(sessionStorage.getItem("userdetails")) || "";
  };

  const logOut = (router) => {
    sessionStorage.removeItem("userdetails");
    router.push({ name: "SignIn" });
    user.value = "";
  };

  getUser();

  return { setUser, user, logOut };
})();

export default userManager;
